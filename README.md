# Mitigating analytical variability through style transfer

## How to reproduce ? 
### Feature extractor

#### Train feature extractor 
python3.10 -u main.py --model feature_extractor --data_dir data --dataset dataset_rh_4classes --labels pipelines --model_save_dir feature_extractor/models --batch_size 64 --lrate 1e-4 --n_epoch 150

#### Download pre-trained model 
TODO

### StarGAN 

#### Train StarGAN 
python3.10 -u main.py --model stargan --mode train --dataset dataset_rh_4classes --labels pipelines --image_size 56 --c_dim 4 --batch_size 16 --data_dir data --sample_dir stargan/samples --log_dir logs --model_save_dir stargan/models

#### Download pre-trained model 
TODO

### CC-DDPM 
#### Train CC-DDPM 

python3.10 -u main.py --model cc_ddpm --mode train --dataset dataset_rh_4classes --labels pipelines --model_save_dir cc_ddpm/models --batch_size 8 --lrate 1e-4 --n_epoch 200 --n_classes 4 --sample_dir cc_ddpm/samples

#### Download pre-trained model 
TODO
