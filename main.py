import os
import argparse 
from stargan import main as stargan_main 
from cc_ddpm import main as ddpm_main 
from cc_ddpm_s import main as ddpm_s_main 
from c_ddpm import main as c_ddpm_main 
from feature_extractor import main as extractor_main

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    ## COMMON ARGS 
    parser.add_argument('--model', type=str, default='stargan')
    parser.add_argument('--data_dir', type=str, default='data')
    parser.add_argument('--dataset', type=str, default='global_dataset')
    parser.add_argument('--labels', type=str, help='conditions for generation',
                        default='pipelines')
    parser.add_argument('--sample_dir', type=str, default='sampling directory')
    parser.add_argument('--model_save_dir', type=str, default='save directory')
    parser.add_argument('--mode', type=str, default='train', choices=['train', 'test', 'transfer'])
    parser.add_argument('--batch_size', type=int, default=32, help='mini-batch size')

    ## STARGAN
    parser.add_argument('--c_dim', type=int, default=5, help='dimension of domain labels (1st dataset)')
    parser.add_argument('--image_size', type=int, default=56, help='image resolution')
    parser.add_argument('--g_conv_dim', type=int, default=64, help='number of conv filters in the first layer of G')
    parser.add_argument('--d_conv_dim', type=int, default=64, help='number of conv filters in the first layer of D')
    parser.add_argument('--g_repeat_num', type=int, default=6, help='number of residual blocks in G')
    parser.add_argument('--d_repeat_num', type=int, default=4, help='number of strided conv layers in D')
    parser.add_argument('--lambda_cls', type=float, default=1, help='weight for domain classification loss')
    parser.add_argument('--lambda_rec', type=float, default=10, help='weight for reconstruction loss')
    parser.add_argument('--lambda_gp', type=float, default=10, help='weight for gradient penalty')
    parser.add_argument('--num_iters', type=int, default=200000, help='number of total iterations for training D')
    parser.add_argument('--num_iters_decay', type=int, default=100000, help='number of iterations for decaying lr')
    parser.add_argument('--g_lr', type=float, default=0.0001, help='learning rate for G')
    parser.add_argument('--d_lr', type=float, default=0.0001, help='learning rate for D')
    parser.add_argument('--n_critic', type=int, default=5, help='number of D updates per each G update')
    parser.add_argument('--beta1', type=float, default=0.5, help='beta1 for Adam optimizer')
    parser.add_argument('--beta2', type=float, default=0.999, help='beta2 for Adam optimizer')
    parser.add_argument('--resume_iters', type=int, default=None, help='resume training from this step')
    parser.add_argument('--test_iters', type=int, default=35000, help='test model from this step')
    parser.add_argument('--model_param', type=str, default='./feature_extractor/models/model_b-64_lr-1e-04_epochs_140.pth', help='model to use for classif')
    parser.add_argument('--log_step', type=int, default=10)
    parser.add_argument('--model_save_step', type=int, default=10000)
    parser.add_argument('--lr_update_step', type=int, default=10000)

    ## DDPM OR FEATURE EXTRACTOR
    parser.add_argument('--n_epoch', type=int, default=500, help='number of total iterations')
    parser.add_argument('--lrate', type=float, default=1e-4, help='learning rate')
    parser.add_argument('--n_feat', type=int, default=64, help='number of features')
    parser.add_argument('--n_classes', type=int, default=24, help='number of classes')
    parser.add_argument('--beta', type=tuple, default=(1e-4, 0.02), help='beta')
    parser.add_argument('--n_T', type=int, default=500, help='number T: nb of timesteps')
    parser.add_argument('--n_C', type=int, default=10, help='number C: nb of images for sampling')
    parser.add_argument('--sampling', type=str, default='random', help='how to sample target img')
    parser.add_argument('--drop_prob', type=float, default=0.1, help='probability drop')
    parser.add_argument('--ws_test', type=int, default=[0.5], help='weight strengh for sampling')
    parser.add_argument('--test_iter', type=int, default=10, help='epoch of model to test')


    config = parser.parse_args()

    print(config)

    if config.model == 'stargan':
        stargan_main.main(config)

    elif config.model == 'c_ddpm':
        if config.mode == 'train':
            c_ddpm_main.train(config)

        elif config.mode == 'transfer':
            c_ddpm_main.transfer(config)

    elif config.model == 'cc_ddpm':
        if config.mode == 'train':
            ddpm_main.train(config)

        elif config.mode == 'transfer':
            ddpm_main.transfer(config)

    elif config.model == 'cc_ddpm_s':
        if config.mode == 'train':
            ddpm_s_main.train(config)

        elif config.mode == 'transfer':
            ddpm_s_main.transfer(config)

    elif config.model == 'feature_extractor':
        extractor_main.main(config)
